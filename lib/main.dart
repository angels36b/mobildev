import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrread/page/home.dart';
import 'package:qrread/API/map_view.dart';
import 'package:qrread/provider/ui_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context){
  return MultiProvider(
    providers: [
      ChangeNotifierProvider(create:
      (_) => new UiProvider() ),
    ],
    child: MaterialApp(
    title: 'QR Reader',
    initialRoute: 'home',
    routes: {
      'home': ( _ )=> HomePage(),
      'mapa': ( _ )=> MapPage(),
    },
       ),
    );
  }

}