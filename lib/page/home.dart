import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrread/API/api_page.dart';
import 'package:qrread/Product/direcciones_page.dart';

import 'package:qrread/provider/db_provider.dart';
import 'package:qrread/provider/ui_provider.dart';

import 'package:qrread/widgets/custom_navigationbar.dart';
import 'package:qrread/widgets/scan_button.dart';

class HomePage extends StatelessWidget{
@override
  Widget build(BuildContext context){

   return Scaffold(
     appBar: AppBar(
       title:  Text('Aplicacion'),
       actions: [
         IconButton(
             icon: Icon(Icons.delete_forever),
             onPressed: (){ }
             ),
           ],
         ),
     body: _HomePageBody(),
     bottomNavigationBar: CustomNavigationBar(),
     floatingActionButton: ScanButton(),
     floatingActionButtonLocation:
      FloatingActionButtonLocation.centerDocked,
   );
}

}

class _HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //obtener el select menu opt
    final uiProvider = Provider.of<UiProvider>(context);

//leer base de datos
    final tempScan = new ScanModel(valor: 'httplalala');
    DBProvider.db.nuevoScan(tempScan);



    final currentIndex = uiProvider.selectedMenuOpt;

  switch( currentIndex ){

    case 0:

      return MapasPage();

    case 1:

      return DireccionesPage();

    default:
      return MapasPage();
  }
  }
}
